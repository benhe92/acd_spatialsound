using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sampleMenu : MonoBehaviour
{
    public AudioClip selectedClip;
    public GameObject menuObj;
    
    public void OpenMenu()
    {
        menuObj.SetActive(true);
    }

    public void CloseMenu()
    {
        menuObj.SetActive(false);
    }



}
