using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playOnButton : MonoBehaviour
{
    public List<AudioClip> samples;
    public int sampleID;
    // Start is called before the first frame update

    void Play()
    {
        AudioSource audioSource = this.gameObject.GetComponent<AudioSource>();
        
        audioSource.clip = samples[sampleID];
        
    }
}
