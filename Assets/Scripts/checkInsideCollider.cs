using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkInsideCollider : MonoBehaviour
{
    public bool zone1Active, zone2Active, zone3Active, zone4Active;

    void Update()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0f);

        foreach(Collider hitCollider in hitColliders)
        {
            if (hitCollider.transform.tag == "zone")
            {
                if(hitCollider.gameObject.name == "zone1")
                {
                    if(zone1Active == true)
                    { 
                        return;
                    }

                    Debug.Log("Setting 1 active");
                    zone1Active = true;
                    zone2Active = false;
                    zone3Active = false;
                    zone4Active = false;
                }

                if (hitCollider.gameObject.name == "zone2")
                {
                    if (zone2Active == true)
                    {
                        return;
                    }
                    Debug.Log("Setting 2 active");
                    zone1Active = false;
                    zone2Active = true;
                    zone3Active = false;
                    zone4Active = false;
                }

                if (hitCollider.gameObject.name == "zone3")
                {
                    if (zone3Active == true)
                    {
                        return;
                    }

                    Debug.Log("Setting 3 active");
                    zone1Active = false;
                    zone2Active = false;
                    zone3Active = true;
                    zone4Active = false;
                }

                if (hitCollider.gameObject.name == "zone4")
                {
                    if (zone4Active == true)
                    {
                        return;
                    }

                    Debug.Log("Setting 4 active");
                    zone1Active = false;
                    zone2Active = false;
                    zone3Active = false;
                    zone4Active = true;
                }
            }
            
        }
    }
}
