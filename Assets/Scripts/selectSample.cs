using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selectSample : MonoBehaviour
{
    public void sendSample()
    {
        AudioClip sample = this.gameObject.GetComponent<AudioSource>().clip;
        GameObject.Find("manager").GetComponent<sampleMenu>().selectedClip = sample;
    }
}
