using UnityEngine;
using System.Collections;

public class SendPositionOnUpdate : MonoBehaviour
{

    public OSC osc;
    public int oscID;
    public bool isColliding;

    // Use this for initialization
    void Start()
    {
        osc = GameObject.Find("manager").GetComponent<OSC>();

    }

    // Update is called once per frame
    void Update()
    {

        OscMessage message = new OscMessage();

        message.address = "/UpdateXYZ";
        message.values.Add(oscID);
        message.values.Add(transform.position.x);
        message.values.Add(transform.position.y);
        message.values.Add(transform.position.z);
        osc.Send(message);

        //message = new OscMessage();
        //message.address = "/UpdateX/" + oscID;
        //message.values.Add(transform.position.x);
        //osc.Send(message);

        //message = new OscMessage();
        //message.address = "/UpdateY/" + oscID;
        //message.values.Add(transform.position.y);
        //osc.Send(message);

        //message = new OscMessage();
        //message.address = "/UpdateZ/" + oscID;
        //message.values.Add(transform.position.z);
        //osc.Send(message);


    }
    IEnumerator Reset()
    {
        yield return new WaitForEndOfFrame();
        isColliding = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (isColliding) return;
        isColliding = true;

        StartCoroutine(Reset());

        OscMessage message = new OscMessage();
        message.address = "/UpdateXYZ";
        message.values.Add(oscID);
        message.values.Add("play");
        //message.values.Add("play");
        osc.Send(message);

    }
    }