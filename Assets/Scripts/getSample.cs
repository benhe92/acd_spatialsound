using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getSample : MonoBehaviour
{
    public AudioClip sample;
    // Start is called before the first frame update
    void Start()
    {
        sample = GameObject.Find("manager").GetComponent<sampleMenu>().selectedClip;
        this.gameObject.GetComponent<AudioSource>().clip = sample;
    }
}
