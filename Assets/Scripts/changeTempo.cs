using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeTempo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetTempo(float newTempo)
    {
        GameObject[] spheres = GameObject.FindGameObjectsWithTag("sphere");

        GameObject.Find("manager").GetComponent<circle>().SetToStartPoint();
        
        for(int i = 0; i < spheres.Length; i++)
        {
            spheres[i].GetComponent<orbitAround>().orbitSpeed = newTempo*1000f;
        }
    }
}
