using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;

public class makeGrid : MonoBehaviour
{
    public GameObject plane;
    Vector3 planePos;
    public float height;
    public float width;

    public List<Vector3> points;
    public Texture tex;

    public List<GameObject> planes;
  
    // Start is called before the first frame update
    void Awake()
    {
        planePos = plane.transform.position;
        height = plane.transform.localScale.z;
        width = plane.transform.localScale.x;

    }
    void Start()
    {
        
        drawLine();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void drawLine()
    {
       
        planes = this.gameObject.GetComponent<spawn>().planes;

        for (int i = 0; i < planes.Count; i++)
        {
            points.Add(planes[i].transform.position);
        }

        VectorLine myLine = new VectorLine("Line", points, tex, 2);
        myLine.Draw();
        
    }
}
