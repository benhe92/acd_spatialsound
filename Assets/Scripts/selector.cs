using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class selector : MonoBehaviour
{
    public Camera camera;
    public GameObject manager;
    public List <GameObject> gridObjs = null;
    public bool selecting = false;
    public GameObject menu;

    public Material tempMaterial, originalMaterial;
    GameObject tempObj;

    public GameObject laserPointerHit;

    int gridLayerIndex;

    int layerMask;

    void Start()
    {
        camera = Camera.main;
        manager = this.gameObject;
        Debug.Log(menu.active);


    }

    void Update()
    {

        //// Bit shift the index of the layer (8) to get a bit mask
        //int layerMask = 1 << 7;

        //// This would cast rays only against colliders in layer 8.
        //// But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        //layerMask = ~layerMask;

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            //Debug.Log(objectHit.gameObject.name);
        
            if(tempObj != null && objectHit != tempObj)
            {
                
                tempObj.GetComponent<MeshRenderer>().material = originalMaterial;
            }

            if(objectHit.gameObject.tag == "gridPoint")
            {
                tempObj = objectHit.gameObject;
                tempObj.GetComponent<MeshRenderer>().material = tempMaterial;
                
                //objectHit.gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
            }
            if(objectHit.gameObject.tag == "gridPoint" && Input.GetMouseButtonUp(0) && menu.activeSelf == false)
            {
                
                selectPoint(objectHit.gameObject);
            }

            if (objectHit.gameObject.tag == "gridPoint" && Input.GetMouseButtonUp(0))
            {
                objectHit.GetComponent<MeshRenderer>().material = tempMaterial;
            }
        }
    }


     void selectPoint(GameObject obj)
    {
        Debug.Log("click");

        if(gridObjs == null)
        {
            
        }

        if(manager.GetComponent<circle>().obj1 == null && !menu.activeSelf)
        {
            
            this.gameObject.GetComponent<sampleMenu>().OpenMenu();

            obj.GetComponent<MeshRenderer>().material = tempMaterial;
            manager.GetComponent<circle>().obj1 = obj;
            

            for (int i = 0; i < gridObjs.Count; i++)
            {
                if (gridObjs[i].transform.position.y != obj.transform.position.y)
                {
                    gridObjs[i].SetActive(false);
                }
            }
            return;
        }

        if (manager.GetComponent<circle>().obj1 != null && manager.GetComponent<circle>().obj2 == null)
        {
            
            manager.GetComponent<circle>().obj2 = obj;
            manager.GetComponent<circle>().getMiddlePoint();

            for (int i = 0; i < gridObjs.Count; i++)
            {
      
               gridObjs[i].SetActive(true);
                
            }

            manager.GetComponent<circle>().obj1 = null;
            manager.GetComponent<circle>().obj2 = null;
            
            return;
        }

        if (manager.GetComponent<circle>().obj1 == null && manager.GetComponent<circle>().obj2 == null)
        {
            manager.GetComponent<circle>().obj1 = null;
            manager.GetComponent<circle>().obj2 = null;
            
            return;
        }


    }
}
