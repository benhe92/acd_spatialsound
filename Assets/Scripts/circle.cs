using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;

public class circle : MonoBehaviour
{
    public int numberOfObjects;
    public float radius;
    public GameObject prefab;
    public Transform centerPos;
    public GameObject obj1;
    public GameObject obj2;
    Vector3 pos1;
    Vector3 pos2;
    public float distance;
    public int orbitLineResolution;
    public Material lineMaterial;

    public bool play = true;
    public GameObject go;
    public Vector3 startPoint;
    public Vector3 mid;
    public float orbitSpeed = 50f;
    public int oscID = 0;

    public List<Vector3> startPoints;
    public List<GameObject> soundSpheres;

    private void Update()
    {
        if(Input.GetMouseButtonUp(1))
        {
            DeleteLast();
        }
    }

    public void getMiddlePoint() 
    {
        pos1 = obj1.transform.position;
        pos2 = obj2.transform.position;

        mid = (pos1 + pos2) / 2;

        distance = Vector3.Distance(pos1, pos2);
        Debug.Log("Center point: " + mid);
        Vector3 angle = pos1 - pos2;
        drawCircle(10, distance / 2, mid, angle);
        Debug.Log("Distance: " + distance);

    }

    public void drawCircle(int num, float rad, Vector3 center, Vector3 angle)
    {


        var orbitLine = new VectorLine("OrbitLine", new List<Vector3>(orbitLineResolution), 2.0f, LineType.Continuous);
        orbitLine.material = lineMaterial;
        var direction = Vector3.Cross(angle, new Vector3(1, 0, 1));


        //// DEBUG LINE
        //List<Vector3> pts = new List<Vector3>();
        //pts.Add(center);
        //pts.Add(center + direction);
        //var line = new VectorLine("line", pts, 5.0f, LineType.Continuous);
        //line.Draw();
        ////

        orbitLine.MakeCircle(center, Vector3.up, rad);
        //orbitLine.MakeCircle(center, direction, rad);
        orbitLine.Draw3DAuto();

        
        startPoint = new Vector3(center.x + rad, center.y, center.z);
        startPoints.Add(startPoint);
        Debug.Log("Start2: " + startPoint);

        go = Instantiate(prefab, startPoint, Quaternion.identity);

        go.GetComponent<orbitAround>().mid = center;
        go.GetComponent<SendPositionOnUpdate>().enabled = true;
        go.GetComponent<SendPositionOnUpdate>().oscID = oscID;
        go.GetComponent<orbitAround>().radius = rad;
        
        soundSpheres.Add(go);
        SetToStartPoint();
        oscID++;
    }

    public void SetToStartPoint()
    {
        for(int i = 0; i < soundSpheres.Count; i++)
        {
            soundSpheres[i].transform.position = startPoints[i];
        }
    }

    public void DeleteLast()
    {
        Destroy(soundSpheres[-1]);
        
        //soundSpheres.Remove(soundSpheres[-1]);
        startPoints.Remove(startPoints[-1]);
    }
}
