using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;


public class parseCSV : MonoBehaviour
{
    public string path;
    public List<Vector3> vec;
    public GameObject gridPrefab;
    public GameObject grid;
    public List<GameObject> gridObjs;

    void Start()
    {
        grid = GameObject.Find("grid");
        vec = new List<Vector3>();

        string path = "Assets/Resources/points.txt";

        var fileLines = System.IO.File.ReadAllLines(path);
        foreach (var line in fileLines)
        {
            var result = line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (result.Length == 3)
            {
                var x = float.Parse(result[0], CultureInfo.InvariantCulture);
                var y = float.Parse(result[1], CultureInfo.InvariantCulture);
                var z = float.Parse(result[2], CultureInfo.InvariantCulture);
                vec.Add(new Vector3(x*10, z*10,y*10));
            }
        }

        for(int i = 0; i < vec.Count; i++)
        {
            
          GameObject gridGo = Instantiate(gridPrefab, vec[i], Quaternion.identity);
          gridGo.transform.parent = grid.transform;
          gridObjs.Add(gridGo);
        }

        this.gameObject.GetComponent<selector>().gridObjs = gridObjs;

    }
}