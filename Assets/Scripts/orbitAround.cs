using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbitAround : MonoBehaviour
{
    public Vector3 mid;
    public float orbitSpeed = 50f;
    public bool play = true;
    public bool isColliding = false;
    public float particleDuration = 1000f;
    GameObject particles;
    public float radius;
    public AudioSource audio;
    public GameObject particlePrefab;

    // Start is called before the first frame update
    void Start()
    {
        audio = this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (play == true)
        {
            this.gameObject.transform.RotateAround(mid, Vector3.up, (orbitSpeed * Time.deltaTime / radius));
        }
    }

    IEnumerator Reset()
    {
        //yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(.5f);
        isColliding = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (isColliding) return;
        isColliding = true;

        //Debug.Log("hit");
        //Debug.Log(other.gameObject.name);
        particles = Instantiate(particlePrefab, this.gameObject.transform.position, Quaternion.identity);
        audio.Play();
        Destroy(particles, 1);
        StartCoroutine(Reset());
        
    }


}
