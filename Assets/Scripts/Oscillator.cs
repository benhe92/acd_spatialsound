﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    float timeCounter=0;
    public float bpm; 
    public float radius;
    public int xOffset;
    public int zOffset;
    public int yOffset;
    public int bar;

    // Start is called before the first frame update
    void Start()
    {
        bpm = Random.Range(0.1f, 1f);
        radius = Random.Range(1.0f, 300f);
        yOffset = Random.Range(10,100);
    }

    // Update is called once per frame
    void Update()
    {
        timeCounter += Time.deltaTime*bpm*bar;

        float x = xOffset + (Mathf.Cos (timeCounter))* radius;
        float y = yOffset;
        float z = zOffset + (Mathf.Sin (timeCounter))* radius; 

        transform.position = new Vector3 (x, y, z);
    }
}
