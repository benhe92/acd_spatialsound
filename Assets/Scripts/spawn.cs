﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    int width = 10;
    int height = 5;
    public GameObject prefab;
    public List<GameObject> planes;
    // Start is called before the first frame update
    void Start()
    {
        for(int x = 1; x < width; x++)
        {
            //Vector3 pos = new Vector3(prefab.transform.position.x * x, prefab.transform.position.y, prefab.transform.position.z);
            //Instantiate(prefab, pos, prefab.transform.rotation);

            for(int y = 1; y < height; y++)
            {
            Vector3 pos = new Vector3(prefab.transform.position.x * x, prefab.transform.position.y * y, prefab.transform.position.z);
            GameObject gridObj =  Instantiate(prefab, pos, prefab.transform.rotation);
            gridObj.transform.parent = gameObject.transform;
            gridObj.tag = "grid";
            planes.Add(gridObj);
            }
        }
        this.gameObject.GetComponent<makeGrid>().drawLine();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
