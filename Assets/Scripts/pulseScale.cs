using UnityEngine;

public class pulseScale : MonoBehaviour
{
    public float speed = 1f;
    void Update()
    {
        Vector3 vec = new Vector3(Mathf.Sin(Time.time*speed) + 1, Mathf.Sin(Time.time * speed) + 1, Mathf.Sin(Time.time * speed) + 1);

        transform.localScale = vec;
    }
}